package com.example.bibinb.samplejsonapplication.netwroks;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.bibinb.samplejsonapplication.interfaces.NetWorkResponce;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bibin.b on 5/26/2017.
 */

public class NetWorkVolley {
    private static final String TAG = "NetWorkVolley";
    // private static final String USER_ID = "userId";

    public static void mVolleyConnection(String url, JSONObject jsonObject, final NetWorkResponce netWorkResponce) throws JSONException {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                netWorkResponce.responce(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                netWorkResponce.error(error.getLocalizedMessage());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue((Context) netWorkResponce);
        requestQueue.add(jsonObjectRequest);

    }
}
