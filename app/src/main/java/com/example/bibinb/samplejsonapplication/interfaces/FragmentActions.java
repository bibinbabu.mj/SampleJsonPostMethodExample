package com.example.bibinb.samplejsonapplication.interfaces;

/**
 * Created by bibin.b on 5/26/2017.
 */

public interface FragmentActions {
    void replace(String fragmentName);
}
