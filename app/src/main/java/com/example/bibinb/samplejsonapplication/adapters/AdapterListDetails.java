package com.example.bibinb.samplejsonapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextClock;
import android.widget.TextView;

import com.example.bibinb.samplejsonapplication.R;
import com.example.bibinb.samplejsonapplication.models.CategoryList;

import java.util.ArrayList;

/**
 * Created by bibin.b on 5/26/2017.
 */

public class AdapterListDetails extends BaseAdapter {
    ArrayList<CategoryList> categoryLists;
    Context context;

    public AdapterListDetails(ArrayList<CategoryList> categoryLists, Context context) {
        this.categoryLists = categoryLists;
        this.context = context;
    }

    @Override
    public int getCount() {
        return categoryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listadpter, parent,false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.ids = (TextView) convertView.findViewById(R.id.user_id);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        CategoryList categoryList = categoryLists.get(position);
        holder.name.setText(categoryList.getCategoryname());
        holder.ids.setText(""+categoryList.getCategoryid());

        return convertView;
    }

    public class ViewHolder {
        TextView name, ids;
    }
}
