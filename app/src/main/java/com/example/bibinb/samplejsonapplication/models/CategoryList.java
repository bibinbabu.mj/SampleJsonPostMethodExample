
package com.example.bibinb.samplejsonapplication.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryList {

    @SerializedName("categoryid")
    @Expose
    private Integer categoryid;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("parentid")
    @Expose
    private Object parentid;
    @SerializedName("description")
    @Expose
    private Object description;

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Object getParentid() {
        return parentid;
    }

    public void setParentid(Object parentid) {
        this.parentid = parentid;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

}
