package com.example.bibinb.samplejsonapplication.interfaces;

import org.json.JSONObject;

/**
 * Created by bibin.b on 5/26/2017.
 */

public interface NetWorkResponce {
    void responce(JSONObject response);

    void error(String localizedMessage);
}
