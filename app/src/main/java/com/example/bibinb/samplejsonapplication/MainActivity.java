package com.example.bibinb.samplejsonapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.bibinb.samplejsonapplication.adapters.AdapterListDetails;
import com.example.bibinb.samplejsonapplication.interfaces.NetWorkResponce;
import com.example.bibinb.samplejsonapplication.models.CategoryList;
import com.example.bibinb.samplejsonapplication.models.Example;
import com.example.bibinb.samplejsonapplication.netwroks.NetWorkVolley;
import com.example.bibinb.samplejsonapplication.uitls.Constants;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NetWorkResponce {
    private static final String TAG = "MainActivity";
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        try {
            mJsonConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void mJsonConnection() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId", "797");
        NetWorkVolley.mVolleyConnection(Constants.URL, jsonObject, MainActivity.this);
    }

    @Override
    public void responce(JSONObject response) {
        try {

            JSONObject item = response;
            ArrayList<CategoryList> categoryLists = new ArrayList<>();
            Log.d(TAG, "responce: " + item.getInt("success"));
            if (item.getInt("success") == 1) {
                JSONObject result = item.getJSONObject("result");
                JSONArray categoryListArray = result.getJSONArray("categoryList");
                for (int i = 0; i < categoryListArray.length(); i++) {
                    CategoryList categoryList = new CategoryList();
                    JSONObject category = categoryListArray.getJSONObject(i);
                    categoryList.setCategoryid(category.getInt("categoryid"));
                   /* categoryList.setParentid(category.getJSONObject("parentid"));
                    categoryList.setDescription(category.getJSONObject("description"));*/
                    categoryList.setCategoryname(category.getString("categoryname"));
                    categoryLists.add(categoryList);
                }
                AdapterListDetails adapterListDetails = new AdapterListDetails(categoryLists, getApplicationContext());
                listView.setAdapter(adapterListDetails);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void error(String localizedMessage) {
        Log.d(TAG, "onErrorResponse() called with: error = [" + localizedMessage + "]");
    }
}
